package showcase.genetic.algorithm;

import showcase.genetic.algorithm.utils.Destiny;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PopulationSelectionAlgorithms {

    // TODO implement tournament selection
    public static <I extends Individual<I>> Stream<I> survivors(
            List<? extends Score<I, ?>> scores, int numberOfSurvivors) {
        return scores.stream()
                .limit(numberOfSurvivors)
                .map(Score::individual);
    }

    public static <I extends Individual<I>> Stream<Population.Parents<I>> parents(
            List<? extends Score<I, ?>> scores, int numberOfBabies) {
        // TODO validate this reproduction model
        return IntStream.range(0, numberOfBabies)
                .mapToObj(i -> parents(scores));
    }

    private static <I extends Individual<I>> Population.Parents<I> parents(List<? extends Score<I, ?>> scores) {
        I firstParent = Destiny.choose(scores).individual();
        I secondParent = Destiny.choose(scores).individual();
        return new Population.Parents<>(firstParent, secondParent);
    }
}
