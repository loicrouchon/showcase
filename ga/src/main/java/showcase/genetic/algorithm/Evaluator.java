package showcase.genetic.algorithm;

public interface Evaluator<I extends Individual<I>, S, A> {

    Score<I, S> evaluate(DataSets<A> dataSets, I individual);

}
