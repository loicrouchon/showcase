package showcase.genetic.algorithm;

import java.util.Comparator;

public class Score<I extends Individual<I>, S> {

    public static final Comparator<Score<?, ?>> COMPARATOR =
            Comparator.<Score<?, ?>, Double>comparing(Score::score).reversed();

    private final double score;

    private final I individual;

    private final S statistics;

    public Score(double score, I individual, S statistics) {
        this.score = score;
        this.individual = individual;
        this.statistics = statistics;
    }

    public double score() {
        return score;
    }

    public String name() {
        return individual().name();
    }

    public I individual() {
        return individual;
    }

    public S statistics() {
        return statistics;
    }
}
