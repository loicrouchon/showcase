package showcase.genetic.algorithm;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class PopulationScores<I extends Individual<I>, S> {

    private final List<Score<I, S>> scores;

    PopulationScores(Stream<Score<I, S>> scores) {
        this.scores = scores.sorted(Score.COMPARATOR).collect(Collectors.toList());
    }

    public List<Score<I, S>> scores() {
        return scores;
    }
}
