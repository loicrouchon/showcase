package showcase.genetic.algorithm;

import java.util.Arrays;
import java.util.List;

public interface Reporter<I extends Individual<I>, S> {

    enum DataType {
        TRAINING, VALIDATION
    }

    void report(DataType dataType, List<Score<I, S>> scores, int iteration);

    static <I extends Individual<I>, S> Reporter<I, S> combine(Reporter<I, S>... reporters) {
        return (dataType, scores, iteration) ->
                Arrays.stream(reporters)
                        .forEach(r -> r.report(dataType, scores, iteration));
    }
}
