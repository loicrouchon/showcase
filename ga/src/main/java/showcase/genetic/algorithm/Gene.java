package showcase.genetic.algorithm;

public interface Gene<G extends Gene<G>> {

    G mutate(double mutationRate);

    default G optimize(DoubleEvaluator<G> evaluator) {
        return (G) this;
    }
}