package showcase.genetic.algorithm;

public interface DoubleEvaluator<T> {

    double evaluate(T element);
}
