package showcase.genetic.algorithm;

public interface Individual<I extends Individual<I>> {

    String name();

    I mutate(double mutationRate);

    I reproduceWith(I secondParent, double crossoverRate);

    default I optimize(DoubleEvaluator<I> evaluator) {
        return (I) this;
    }
}
