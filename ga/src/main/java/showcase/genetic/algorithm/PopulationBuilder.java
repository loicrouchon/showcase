package showcase.genetic.algorithm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import showcase.genetic.algorithm.utils.Decimals;
import showcase.genetic.algorithm.utils.Range;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PopulationBuilder<I extends Individual<I>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PopulationBuilder.class);

    private int nbIndividuals = 30;
    private double elitismRate = 0.05d;
    private double survivalRate = 0.55d;
    private double mutationRate = 0.03d;
    private double crossoverRate = 0.80d;
    private Population.SurvivorSelector<I> survivorsSelector = PopulationSelectionAlgorithms::survivors;
    private Population.ParentsSelector<I> parentsSelector = PopulationSelectionAlgorithms::parents;

    /**
     * @param nbIndividuals number of individuals in the population
     */
    public PopulationBuilder<I> withNbIndividuals(int nbIndividuals) {
        this.nbIndividuals = nbIndividuals;
        return this;
    }

    /**
     * @param elitismRate between 0 and 1, percentage of best individuals to keep as is at each evolution
     */
    public PopulationBuilder<I> withElitismRate(double elitismRate) {
        this.elitismRate = elitismRate;
        return this;
    }

    /**
     * @param survivalRate between 0 and 1, percentage of survivors at each evolution
     */
    public PopulationBuilder<I> withSurvivalRate(double survivalRate) {
        this.survivalRate = survivalRate;
        return this;
    }

    /**
     * @param mutationRate between 0 and 1, the probability of a gene build block mutation
     */
    public PopulationBuilder<I> withMutationRate(double mutationRate) {
        this.mutationRate = mutationRate;
        return this;
    }

    /**
     * @param crossoverRate between 0 and 1, the probability of a gene build block to be chosen from
     *                      first parent instead of second parent
     */
    public PopulationBuilder<I> withCrossoverRate(double crossoverRate) {
        this.crossoverRate = crossoverRate;
        return this;
    }

    public PopulationBuilder<I> withSurvivorsSelector(Population.SurvivorSelector<I> survivorsSelector) {
        this.survivorsSelector = survivorsSelector;
        return this;
    }

    public PopulationBuilder<I> withParentsSelector(Population.ParentsSelector<I> parentsSelector) {
        this.parentsSelector = parentsSelector;
        return this;
    }

    public Population<I> initialize(Supplier<I> individualCreator) {
        LOGGER.info("Generating the initial population of {} individuals, elitismRate: {}, survivalRate: {}, " +
                        "mutationRate: {}, survivorsSelector: {}, parentsSelector: {}", nbIndividuals,
                Decimals.percentage(elitismRate),
                Decimals.percentage(survivalRate),
                Decimals.percentage(mutationRate),
                survivorsSelector.toString(),
                parentsSelector.toString()
        );
        List<I> individuals = IntStream
                .range(0, nbIndividuals)
                .mapToObj(i -> individualCreator.get())
                .collect(Collectors.toList());
        return population(individuals);
    }

    private Population<I> population(List<I> individuals) {
        Range.inRange("Elitism Rate", elitismRate, 0d, 1d);
        Range.inRange("Survival Rate", survivalRate, 0d, 1d);
        Range.inRange("Elitism Rate + Survival Rate", elitismRate + survivalRate, 0d, 1d);
        Range.inRange("Mutation Rate", mutationRate, 0d, 1d);
        Range.inRange("Crossover Rate", crossoverRate, 0d, 1d);
        int numberOfElitesSurvivors = (int) Math.floor(individuals.size() * elitismRate);
        int numberOfSurvivors = (int) Math.max(1, Math.floor(individuals.size() * survivalRate));
        return new Population<>(individuals, numberOfElitesSurvivors, numberOfSurvivors, mutationRate, crossoverRate,
                survivorsSelector, parentsSelector);
    }
}
