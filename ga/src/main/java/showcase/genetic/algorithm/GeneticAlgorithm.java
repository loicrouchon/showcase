package showcase.genetic.algorithm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

/**
 * http://www.obitko.com/tutorials/genetic-algorithms/
 * http://www.baeldung.com/java-genetic-algorithm
 */
public class GeneticAlgorithm<I extends Individual<I>, S, A> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneticAlgorithm.class);

    private final PopulationBuilder<I> populationBuilder;
    private final Supplier<I> incubator;

    public GeneticAlgorithm(PopulationBuilder<I> populationBuilder, Supplier<I> incubator) {
        this.populationBuilder = populationBuilder;
        this.incubator = incubator;
    }

    public Population<I> train(Evaluator<I, S, A> evaluator, Reporter<I, S> reporter,
                               int optimizeEveryIterations, int iterations,
                               DataSets<A> trainingDataSets, DataSets<A> validationDataSets) {
        Population<I> population = populationBuilder.initialize(incubator);
        LOGGER.info("Training a genetic algorithm on {} iterations", iterations);
        for (int i = 0; i < iterations; i++) {
            if (optimizeEveryIterations > 0 && i % optimizeEveryIterations == 0) {
                LOGGER.info("Optimizing population at iteration {}", i);
                population = population.optimize(trainingDataSets, evaluator);
            }

            LOGGER.info("Evaluating population at iteration {}", i);
            PopulationScores<I, S> trainingEvaluationResult = population.evaluate(trainingDataSets, evaluator);
            PopulationScores<I, S> validationEvaluationResult = population.evaluate(validationDataSets, evaluator);

            // TODO migrate reporter to PopulationScores type
            reporter.report(Reporter.DataType.TRAINING, trainingEvaluationResult.scores(), i);
            reporter.report(Reporter.DataType.VALIDATION, validationEvaluationResult.scores(), i);

            LOGGER.info("Evolving population at iteration {}", i);
            population = population.nextGeneration(trainingEvaluationResult);
        }
        LOGGER.info("Evolution is done!");
        return population;
    }
}
