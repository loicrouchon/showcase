package showcase.genetic.algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Population<I extends Individual<I>> {

    interface SurvivorSelector<I extends Individual<I>> {
        Stream<I> select(List<? extends Score<I, ?>> scores, int numberOfSurvivors);
    }

    interface ParentsSelector<I extends Individual<I>> {
        Stream<Parents<I>> select(List<? extends Score<I, ?>> scores, int numberOfBabies);
    }

    static class Parents<I extends Individual<I>> {
        private final I firstParent;
        private final I secondParent;

        Parents(I firstParent, I secondParent) {
            this.firstParent = firstParent;
            this.secondParent = secondParent;
        }

        I conceive(double crossoverRate) {
            return firstParent.reproduceWith(secondParent, crossoverRate);
        }
    }

    private final List<I> individuals;
    private final int numberOfElitesSurvivors;
    private final int numberOfSurvivors;
    private final double mutationRate;
    private final double crossoverRate;
    private final SurvivorSelector<I> survivorsSelector;
    private final ParentsSelector<I> parentsSelector;

    Population(List<I> individuals, int numberOfElitesSurvivors, int numberOfSurvivors,
               double mutationRate, double crossoverRate,
               SurvivorSelector<I> survivorsSelector, ParentsSelector<I> parentsSelector) {
        this.individuals = individuals;
        this.numberOfElitesSurvivors = numberOfElitesSurvivors;
        this.numberOfSurvivors = numberOfSurvivors;
        this.mutationRate = mutationRate;
        this.crossoverRate = crossoverRate;
        this.survivorsSelector = survivorsSelector;
        this.parentsSelector = parentsSelector;
    }

    <S, A> Population<I> optimize(DataSets<A> dataSets, Evaluator<I, S, A> evaluator) {
        DoubleEvaluator<I> scoreEvaluationFunction = i -> evaluator.evaluate(dataSets, i).score();
        List<I> newGeneration = individuals.parallelStream()
                .map(i -> i.optimize(scoreEvaluationFunction))
                .collect(Collectors.toList());
        return newGeneration(newGeneration);
    }

    <S, A> PopulationScores<I, S> evaluate(DataSets<A> dataSets, Evaluator<I, S, A> evaluator) {
        Stream<Score<I, S>> scores = individuals.parallelStream()
                .map(i -> evaluator.evaluate(dataSets, i));
        return new PopulationScores<>(scores);
    }

    Population<I> nextGeneration(PopulationScores<I, ?> evaluationResult) {
        List<? extends Score<I, ?>> scores = evaluationResult.scores();
        List<I> newGeneration = new ArrayList<>(scores.size());
        elites(scores).forEach(newGeneration::add);
        mutate(survivors(scores), mutationRate).forEach(newGeneration::add);
        babies(scores, scores.size() - newGeneration.size()).forEach(newGeneration::add);
        return newGeneration(newGeneration);
    }

    private Stream<I> elites(List<? extends Score<I, ?>> scores) {
        return scores.stream()
                .limit(numberOfElitesSurvivors)
                .map(Score::individual);
    }

    private Stream<I> survivors(List<? extends Score<I, ?>> scores) {
        return survivorsSelector.select(scores, numberOfSurvivors);
    }

    private Stream<I> mutate(Stream<I> survivors, double mutationRate) {
        return survivors.map(i -> i.mutate(mutationRate));
    }

    private Stream<I> babies(List<? extends Score<I, ?>> scores, int nbBabies) {
        return parentsSelector.select(scores, nbBabies)
                .map(p -> p.conceive(crossoverRate));
    }

    private Population<I> newGeneration(List<I> newGeneration) {
        return new Population<>(newGeneration, numberOfElitesSurvivors, numberOfSurvivors, mutationRate, crossoverRate,
                survivorsSelector, parentsSelector);
    }
}
