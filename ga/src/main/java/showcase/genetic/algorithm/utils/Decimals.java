package showcase.genetic.algorithm.utils;

import java.util.Locale;

public class Decimals {

    private static final String PERCENTAGE_FORMAT = "%7.3f";

    public static String percentage(double value) {
        return String.format(Locale.US, PERCENTAGE_FORMAT, value * 100) + '%';
    }
}
