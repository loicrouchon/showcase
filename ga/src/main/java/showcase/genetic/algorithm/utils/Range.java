package showcase.genetic.algorithm.utils;

public class Range {

    public static double inRange(String name, double value, double lowerBound, double upperBound) {
        if (lowerBound <= value && value <= upperBound) {
            return value;
        }
        throw new IllegalArgumentException(name + " " + value + " should be in " +
                "interval [" + lowerBound + ", " + upperBound + "]");
    }
}
