package showcase.genetic.algorithm.utils;

import java.util.List;
import java.util.Random;

public class Destiny {

    private static final Random random = new Random();

    public static <T> T choose(List<T> list) {
        return list.get(random.nextInt(list.size()));
    }
}
